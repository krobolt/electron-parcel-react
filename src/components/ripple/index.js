/**
 * Useage
 * let r = new Ripple(document.querySelector(".ripple-effect"))
 */

class Ripple{
    constructor(ele){
        for (let er of ele){    
            er.addEventListener('click',  animate)
        }  
    }  
}

function animate(e){
    var w = this.offsetWidth,h = this.offsetHeight;
    w = Math.max(w,h);
    var poX = e.x - this.offsetLeft - w/2,
        poY = e.y - this.offsetTop -w/2;
    var ripple = document.createElement('div');
    ripple.classList.add('circle');
    ripple.style.width = ripple.style.height = w+"px";
    ripple.style.left = poX+"px";
    ripple.style.top = poY+"px";
    this.appendChild(ripple);
    setTimeout(function(){
        var ri = document.querySelector(".circle");
        ri.parentElement.removeChild(ri)
    },600);
}

export {Ripple}

