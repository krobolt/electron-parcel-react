//NewStatusBar from Higer order functions
function NewStatusBar(StatusMenu){
    let bars = document.createElement('div');
    bars.classList = 'bars';
    bars.appendChild(StatusMenu());
    return bars
}

//create mac items
function MacStatusItems(){
    let u = document.createElement('ul')
    let l = document.createElement('li')    
    l.classList = "fas fa-sync status-icon";
    l.innerHTML = "Online"; 
    u.appendChild(l);
    return u;    
}

function NewMacStatusBar(){
    return NewStatusBar(
        MacStatusItems
    )
}

function CreateStatusBar(platform){  
    let status = document.getElementById('statusbar');
    switch(String(platform)){
        case "darwin":            
            status.appendChild(NewMacStatusBar())
            break;
        case "linux":
            status.appendChild(NewMacStatusBar())
            break;
        default:
            status.appendChild(NewMacStatusBar())
    }
};

export {CreateStatusBar}