const remote = require('electron').remote;

export default () => {

var ws = new WebSocket("ws://localhost:6060/echo");

ws.onopen = function() {
    document.getElementById('statusbar').classList = "connected"
    ws.send("/ping");
    ws.send("/list");
    console.log("Message is sent...")
};

// Get the input field
var input = document.getElementById("send");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Cancel the default action, if needed
  event.preventDefault();
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Trigger the button element with a click
    document.getElementById("sendsubmit").click();
  }
}); 


$('#sendsubmit').on('click',function(){
    var i = document.getElementById("send")

    if (i.value == ""){
        return
    }

    ws.send(i.value)
    addMsg("you: "+i.value)
    i.value = ""
    i.focus()

    let objDiv = document.getElementById('main-view-container');
    objDiv.scrollTop = objDiv.scrollHeight;

})


ws.onmessage = function (evt) {
   var received_msg = evt.data;
   console.log(received_msg)
 
   let json = JSON.parse(received_msg);

   console.log(received_msg)
   console.log(json)

   let list = document.getElementById("connections")

   if (json.code == 1){
    console.log('LIST COMMAND.....')
    list.innerHTML = ""

    for (var k in json.data){
        let listconn = document.createElement("li")            
        listconn.innerHTML = json.data[k]
        listconn.setAttribute('id',json.data[k])
        list.appendChild(listconn)                
    }
   }

   if (json.code == 2){
    console.log('ADD COMMAND.....')

   let listconn = document.createElement("li")            
        listconn.innerHTML = json.data[0]
        listconn.setAttribute('id',json.data[0])
        list.appendChild(listconn)      
   }

   if (json.code == 3){
    console.log('REMOVE COMMAND.....')
    let l = document.getElementById(json.data[0]);
    l.remove();    
   }

   
   if (json.code == 4){
    console.log('MSG COMMAND.....')
    addMsg(json.data[0]+ ": "+json.data[1])
   }


};




function addMsg(s){
    let chats = document.querySelectorAll('.chatdistplay.active');
    let d = document.createElement('div');
    d.innerHTML = s;
    chats[0].appendChild(d);
}


ws.onclose = function() {
   // websocket is closed.
   console.log("Connection is closed...")
   document.getElementById('statusbar').classList = "disconnected"
};



};