class Sidebar{
    constructor(sidebar, screenVal) {

        this.screen = screenVal;
        this.events = {
            mousedown: false,           
        }

        let dragbar = sidebar.querySelector('.dragbar');    
        if (dragbar == null){
            throw Error("unable to find dragbar")
        }
    
        const moveEvent = function(e){
            this.MouseMove(e)
        }.bind(this);
        
        dragbar.addEventListener('mousedown', (e) => {
            e.preventDefault();   
            this.events.mousedown = true;          
            sidebar.classList.add("active")
            document.addEventListener('mousemove', moveEvent, false);
        }, false);

        document.addEventListener('mouseup', (e) => {
            this.events.mousedown = false;       
            document.removeEventListener('mousemove', moveEvent, false);    
            document.querySelectorAll('.horizontal-resize.active').forEach((div) => {
                RemoveActive(div)
            });       
        });
        
    }
    MouseMove(e){    
        let activeSidebar = document.querySelectorAll('.horizontal-resize.active')
        if (activeSidebar.length < 1){        
            return;
        }
    
        let side = activeSidebar[0].querySelector('.side');
        let left = side.getBoundingClientRect().left    

        let active = {
            min: activeSidebar[0].getAttribute('data-min'),
            max: activeSidebar[0].getAttribute('data-max'),
            side: activeSidebar[0].querySelector('.side'),
            main:  activeSidebar[0].querySelector(".main"),
            left: left,
            shift: e.pageX - left+2
        };
        
        if (active.shift < active.min){
            active.shift = 0
        }
    
        if (active.shift > this.screen * active.max - active.left){
            active.shift = this.screen * active.max - active.left
        }
    
        Update(active);
    }
}


//Update view add styles.
function Update(activeSidebar){
    activeSidebar.side.setAttribute("style", "width:"+activeSidebar.shift+"px;");
    activeSidebar.main.setAttribute("style", "left:"+activeSidebar.shift+"px;");
    return activeSidebar
}

function RemoveActive(div){  
    div.classList.remove("active")
    return div   
}


export {Sidebar, RemoveActive, Update}
