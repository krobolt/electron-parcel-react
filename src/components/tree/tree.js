
function FetchContentsExample(path){
    return [NewFile("name","path/to/file",false)]
}

function NewFile(n,p,d){
    return{
        name: n,
        path: p,
        dir: d,
    }
}

function CreateList(path, FetchContents){
    let frag = document.createElement('ul');
    let files = FetchContents(path)
    frag.setAttribute('data-path',path)    
    for (let f of files){
        let li = document.createElement('li')            
        let sp = document.createElement('span')
        sp.innerHTML = f.name                            
        li.setAttribute('data-path',f.path)
        if (f.dir){
            li.classList = 'dir'
            sp.addEventListener('click',(e)=>{                        
                if (li.classList.contains('loaded')){
                    li.classList.toggle('active')
                    return;
                }
                li.appendChild(CreateList(f.path, FetchContents))
                li.classList.add('loaded','active')            
            });
        }else{
            li.classList = 'file'
        }
        li.appendChild(sp)
        frag.appendChild(li)
    }
    return frag;   
}


function NewTree(dst, path, fetcher){
    dst.innerHTML = ""
    dst.appendChild(CreateList(path,fetcher))    
}

export {NewTree, CreateList, NewFile}
