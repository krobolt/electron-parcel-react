/**
 * 
 * Drop down accordion
 * 
 * Heading.         (hide)
 * - List A
 * - List B
 * - List C 
 * 
 * Useage. Sidebar. Eg. Mac : Network/Places etc.
 *                      Win : MyPC, Documents, Network....
 * 
 */
class Accordion{
    constructor(ele) {
        //for each .class 
        for (el of ele){
            el.addEventListener('click', (e) => {
                e.preventDefault();             
                el.classList.toggle("active")            
            }, false);
        }        
    }
}
export {Accordion}
