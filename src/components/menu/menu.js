const remote = require('electron').remote;
// const store = remote.getGlobal('AppStore'); //app storage.

//Create New Menu Li Element
function _NewMenuItem(icon, id, html,classList){
    let l = document.createElement('li');   
    if (icon !== null){
        l.setAttribute('data-icon',icon)
    }
    if (typeof id != "undefined"){
        l.setAttribute('id',id)
    }
    if (typeof html != "undefined"){
        l.innerHTML = html
    }
    if (typeof classList != "undefined"){
        l.classList = classList
    }
    l.addEventListener('mouseover',() => {
        let others = document.querySelectorAll('.menuitem')
        for (let o of others){
            o.classList.remove('active')            
        }
        l.classList.add('active');
    })
    return l
}

//Create Minimise Controls
function CreateMinControls(){
    let min = _NewMenuItem("–","min-btn")
    min.addEventListener("click", (e) => {
        e.preventDefault();
        var window = remote.getCurrentWindow();
        window.minimize(); 
    });

    min.addEventListener('mouseover',() => {
        min.classList.add('active');
    })

    min.addEventListener('mouseout',() => {
        min.classList.remove('active');
    })

    return min
}

let last = {}

//Create Mazimise Controls
function CreateMaxControls(){
    let max = _NewMenuItem("□","max-btn")
    max.addEventListener("click", (e) => {
        e.preventDefault();
        var window = remote.getCurrentWindow();
        document.body.classList.toggle('maxed')
        if (document.body.classList.contains('maxed')){
            window.maximize();          
        }else{
            window.unmaximize();
        }
    });
    return max
}

//Create Close Controls
function CreateCloseControls(){
    let close = _NewMenuItem("✕","close-btn")    
    close.addEventListener("click", (e) => {
        e.preventDefault();
        var window = remote.getCurrentWindow();
        window.close();
    }); 
    return close
}

function NewMenu( MenuItems, title ){
    let menu = document.createElement('div');
    let bars = document.createElement('div');
    menu.classList = "menu";
    menu.setAttribute('data-title',title)
    bars.classList = "bars";
    menu.appendChild(MenuItems())
    bars.appendChild(menu)
    return bars
}


function WindowsMenuItems(){
    let u = document.createElement('ul');
    //include custome menu
    let l = _NewMenuItem("icon","file-id", "File","menuitem")
        let sub = document.createElement('ul');
        let subl = _NewMenuItem("icon","download-id", "check for updates")
        sub.appendChild(subl)


        l.appendChild(sub)
    u.appendChild(l)
 

    let view = _NewMenuItem("icon-help","view-id", "View","menuitem")
        let vsub = document.createElement('ul');
        let fullscreen = _NewMenuItem("icon",null, "Fullscreen")
        let viewstatus = _NewMenuItem("icon",null, "View Status Bar")
        let sidebar = _NewMenuItem("icon",null, "Hide Sidebar")

        sidebar.addEventListener("click", (e) => {
            e.preventDefault();
            let activeSidebar = document.querySelector('.horizontal-resize')
            let active = {
                side: activeSidebar.querySelector('.side'),
                main:  activeSidebar.querySelector(".main"),
                shift: 0
            };

            if (active.side.classList.contains('hidden')){
                active.shift = 200
                sidebar.innerHTML = 'Hide Sidebar'
            }else{
                sidebar.innerHTML = 'View Sidebar'
            }

            active.side.setAttribute("style", "width:"+active.shift+"px;");
            active.main.setAttribute("style", "left:"+active.shift+"px;");
            active.side.classList.toggle('hidden');
        }); 

        viewstatus.addEventListener("click", (e) => {        
            e.preventDefault();
            let sb = document.getElementById('status-bar');
            viewstatus.classList.toggle('hide')
            sb.classList.toggle('enabled');
        }); 

        fullscreen.addEventListener("click", (e) => {
            e.preventDefault();
            var window = remote.getCurrentWindow();
            window.setFullScreen(true);
        }); 

        vsub.appendChild(fullscreen)
        vsub.appendChild(viewstatus)
        vsub.appendChild(sidebar)
        view.appendChild(vsub)
    u.appendChild(view)  

    let help = _NewMenuItem("icon-help","help-id", "Help","menuitem")
    u.appendChild(help)    


    u.appendChild(CreateCloseControls())
    u.appendChild(CreateMaxControls())   
    u.appendChild(CreateMinControls())

    //activate menu 
    u.addEventListener('click', () => {
        u.classList.add('active');
       
    });

   
    //disable menu
    document.addEventListener('click', (e) => {
        

        if (!e.target.matches( '.bars *')) {

            let menui = document.querySelectorAll('.menuitem')
            for(let m of menui){
                m.classList.remove('active');
            }
    
            u.classList.remove('active');

            for (let t of u.children){
                if (typeof t.children[0] !== 'undefined'){
                    t.children[0].classList.remove('active');                    
                }
            }  
        }     
         
    });
    return u;
}



function LinuxMenuItems(){
    let u = document.createElement('ul');
    u.appendChild(CreateMaxControls())
    u.appendChild(CreateMinControls())
    u.appendChild(CreateCloseControls())
    return u;
}

function MacMenuItems(){
    let u = document.createElement('ul');
    u.appendChild(CreateCloseControls())
    u.appendChild(CreateMinControls())
    u.appendChild(CreateMaxControls())
    return u;
}


function NewWindowsMenu(){
    return NewMenu(
        WindowsMenuItems,
        'Some title for this project'
    );
}
function NewLinuxMenu(){
    return NewMenu(
        LinuxMenuItems,
        'Some title for this project'
    );
}
function NewMacMenu(){
    return NewMenu(
        MacMenuItems,
        'Some title for this project'
    );
}


export default (platform) => {  
    var menuarea = document.getElementById("menuarea")
    switch(String(platform)){
        case "darwin":
            console.log('target mac')
            menuarea.appendChild(NewMacMenu())           
            break;
        case "linux":
            console.log('target linux')
            menuarea.appendChild(NewLinuxMenu())
            break;
        default:
            console.log('target windows')
            menuarea.appendChild(NewWindowsMenu())
    }
};