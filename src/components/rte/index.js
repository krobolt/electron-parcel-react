
require('../../../node_modules/quill/dist/quill.snow.css');
import Quill from 'quill';

function NewQuillRTE() {
    let container = document.getElementById('editor');
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],      
        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction    
        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],    
        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],      
        ['clean']                                         // remove formatting button
      ];

    let options = {
       
        theme: 'snow',
    };

    let editor = new Quill(container, options);

    $('.ql-editor').on('keyup', function() {
        let about = document.querySelector('input[name=body]');
        let rawHtml = editor.container.firstChild.innerHTML; // with formatting errors;
        let out = rawHtml.replace(/<p><br><\/p>/g, '');
        about.value = out;
    });

    return editor;
}

export {NewQuillRTE}