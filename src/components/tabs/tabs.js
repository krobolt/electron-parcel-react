//Tab selection

class Tabs{
    constructor(tabs){
        tabs.forEach( (tab) => {
            tab.addEventListener("click", tevent );
        });
    }
}

function tevent(e){
    e.preventDefault();
    if (this.classList.contains('disabled')){
        return;
    }
    let name = this.getAttribute('data-name')
    let t = this.getAttribute('data-target')
    let targets = document.querySelectorAll(t);
    for(let ele of targets){
        if (name === ele.getAttribute('data-name')){
            ele.classList.add('active')
        }else{
            ele.classList.remove('active')
        }
    }    
    _cleartabs()
    this.classList.add("active")
}

function _cleartabs(){
    let tabs = document.querySelectorAll('.tab');
    tabs.forEach( (tab) => {
        if (tab.classList.contains("active")){
            tab.classList.remove("active")
        }
    });
}

export {Tabs}
