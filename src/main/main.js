const {app, BrowserWindow, Menu, dialog, ipcMain} = require('electron')
import * as path from 'path'
import { format as formatUrl } from 'url'
import {BootElectron} from '../electron/browserWindow'
import {AutoUpdate} from '../electron/autoupdate'
const store = require('../electron/filestore/store')


/**
 * Global Storage
 */
store.set('foobar','bazbat');
global.AppStore = store;


/**
 * Communicate between main/renderer
 */
ipcMain.on('dom', (event, arg) => {  
  console.log('dom stufff');
  //event.sender.send('async-reply', 2);
});

ipcMain.on('sync', (event, arg) => {  
  console.log(arg);
  event.returnValue = 4;
  //mainWindow.webContents.send('ping', 5);
});




const isDevelopment = process.env.NODE_ENV !== 'production'

if (isDevelopment == true){
  console.log('developer mode')
  require('electron-reload')(__dirname+'/build', {
    // Note that the path to electron may vary according to the main file
    electron: require(`${__dirname}/node_modules/electron`)
  });
}

app.on('ready', () => {
  //console.log(dialog.showOpenDialog({properties: ['openFile', 'openDirectory', 'multiSelections']}))
  CreateMacMenu();
  BootElectron(2000,store);
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
 BootElectron(2000,store);
});


function CreateMacMenu(){
  //let { width, height } = store.get('windowBounds');
  if (process.platform === 'darwin') {  
    console.log("CREATING MENU")
    const menuTemplate = [
      {
          label: "File",
          submenu: [{
            label: 'Check for updates...',
            role: 'TODO'
          }]
      },
      {
          label: "View",
          submenu: [{
            label: 'View Sidebar',
            role: 'TODO',
            click () { require('electron').shell.openExternal('https://electronjs.org') }
          },
          {
            label: 'View Status Bar',
            role: 'TODO',
            click () { 



            }
            
          }]
      },
      {
          label: "Menu2",
          submenu: [{role: 'TODO'}]
      }
  ];
  
    const menu = Menu.buildFromTemplate(menuTemplate)
    Menu.setApplicationMenu(menu)
  
  }
}