require('../../styles/main.scss');
var electron = require('electron');
var mainWin = electron.remote.getCurrentWindow();

var {ipcRenderer} = require('electron');
import { platform } from 'os';
import {Sidebar} from '../components/sidebar'
import {Tabs} from '../components/tabs/tabs';
import Menu from '../components/menu/menu';
import {NewFileTree} from '../electron/filetree/filetree';
import {Ripple} from "../components/ripple"
import {NewQuillRTE} from "../components/rte"
import React from 'react';
import ReactDOM from 'react-dom';
import {Board} from '../react/board';


let themeswap = document.getElementById('settings-theme-select')
themeswap.addEventListener('change',(e)=>{
    document.body.classList.remove('dark','light')
    let mode = themeswap.value
    console.log(mode);

    if (mode == 'dark'){
        mainWin.setVibrancy('ultra-dark');
    }

    document.body.classList.add(mode);
})


//switch stylesheet for different platforms
switch(String(platform)){
    case "darwin":
        document.body.classList = 'mac light'
        break;
    case "linux":
        document.body.classList = 'linux'
        break;
    default:
        document.body.classList = 'win32 light'
}

if (document.body.classList.contains('dark')){
    //set vibrancy based on theme.
    //mac only. dark.
    mainWin.setVibrancy('ultra-dark');
}


//Example using react with compontents.
ReactDOM.render(
    <Board />,
    document.getElementById('reacttest')
);


//set up ui, sidebars, tabs, menu...
let sidebars = document.querySelectorAll('.horizontal-resize')
sidebars.forEach((sb) => {
    new Sidebar(sb, window.innerWidth)
});

new Tabs(document.querySelectorAll('.tab'))
new Ripple(document.querySelectorAll(".ripple-effect"))

Menu(platform);
NewQuillRTE();
NewFileTree();

//send/receive messages from main.js/node)
ipcRenderer.on('dom', (event, arg) => {  
    console.log('received dom event from main.js');
    console.log(arg);
    console.log(event);    
});
