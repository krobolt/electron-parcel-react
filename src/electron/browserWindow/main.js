const {BrowserWindow} = require('electron')

const mainConfig = {
    width: 800,
    height: 600,
    minHeight: 320,
    minWidth: 420,
    show: false,
    transparent: true, 
    vibrancy: 'appearance-based',
    frame: false,
    webPreferences: {
        nodeIntegrationInWorker: true
    }
}


let electronWindow = null;
let write = true

function MainStorage(store){
    let autoSave = setInterval(function(){
        console.log('saving to file');
        console.log('write allowed: ',write);
        store.set('test','fooba22r');
    },10000)
}

function MainEvents(browserWin){
    browserWin.on('page-title-updated', (e) => {
        e.preventDefault()
    });
    browserWin.on('blur', () => {
        browserWin.webContents.executeJavaScript('document.body.classList.add("window-inactive")', true)
    });
    browserWin.on('focus', () => {
        browserWin.webContents.executeJavaScript('document.body.classList.remove("window-inactive")', true)
    });    
}

function NewMainWindow(path, store){
    electronWindow = new BrowserWindow(mainConfig);  
    MainEvents(electronWindow)
   
    //electronWindow.webContents.openDevTools();

    electronWindow.webContents.on('devtools-opened', () => {
        electronWindow.focus()
        setImmediate(() => {
            electronWindow.focus()
        });
    });

    electronWindow.loadURL(path)

    electronWindow.on('resize', () => {
        write = false;        
        setTimeout(function(){
            write = true
        },2000)
        console.log(electronWindow.getBounds())        
    });
    MainStorage(store)


    return electronWindow
}


export {MainEvents, NewMainWindow}