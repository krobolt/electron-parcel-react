import {NewSplashWindow} from "./spash"
import {NewMainWindow} from "./main"

let electronWindow = null;
let splash = null;

function ShowMainWindow(splash, electronWindow, delay){    
    electronWindow.once('ready-to-show', () => {
        setTimeout(() => {
            electronWindow.show()
            splash.destroy()
            electronWindow.webContents.send('dom', 5);
        }, delay);
    });
}

function BootElectron(delay, store){
    if (electronWindow == null){
        splash = NewSplashWindow('file://' + __dirname + '/views/splash.html');
        electronWindow = NewMainWindow('file://' + __dirname + '/build/index.html', store);
        ShowMainWindow(splash, electronWindow, delay);
    }   
}

export {BootElectron}