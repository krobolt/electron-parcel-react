const {BrowserWindow} = require('electron')

function NewSplashWindow(path){
    let splash = new BrowserWindow({
      width: 480,
      height: 320,
      show: false,
      transparent: true,      
      titleBarStyle: 'customButtonsOnHover', 
      frame:false,
      title: false
    });
  
    splash.on('closed', () => {
      splash = null;
    });
  
    splash.once('ready-to-show', () => {
      splash.show();
    });
    //splash.loadURL('file://' + __dirname + '/views/splash.html');
    splash.loadURL(path);
    return splash;
}
 


export {NewSplashWindow}