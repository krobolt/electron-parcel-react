const electron = require('electron');
const path = require('path');
const fs = require('fs');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');

class Store {
  constructor(opts) {
    if (typeof opts.path == 'undefined'){
      opts.path = userDataPath;
    }
    this.path = path.join(opts.path, opts.configName + '.json');
    this.data = parseDataFile(this.path, opts.defaults);
  }
  
  get(key) {
    return this.data[key];
  }
  
  set(key, val) {
    this.data[key] = val;
    fs.writeFileSync(this.path, JSON.stringify(this.data));
  }
}

function parseDataFile(filePath, defaults) {
  try {
    return JSON.parse(fs.readFileSync(filePath));
  } catch(error) {
    return defaults;
  }
}

// expose the class
module.exports = Store;
export {Store as default}