
const path = require('path');
const fs = require('fs');
const remote = require('electron').remote;
const {dialog} = require('electron').remote

const BrowserWindow = remote.BrowserWindow
let userDataPath = remote.app.getPath('userData');
// In main process.
const ipcMain = require('electron').ipcMain;

// In renderer process (web page).
const ipcRenderer = require('electron').ipcRenderer;


function GetListing(dirPath){
    let frag = document.createElement('ul');
    frag.setAttribute('data-path',dirPath)    
    fs.readdir(dirPath,(err, files) => {        
        if (err){
            return;
        }
        for(let file of files){
            let filePath = path.join(dirPath, file); 
            fs.stat(filePath, (err,stat) => {
                if (err){
                    return;
                }
 
                let li = document.createElement('li')            
                li.setAttribute('data-path',filePath)

                let sp = document.createElement('span')
                sp.innerHTML = file                                    

                if (stat.isDirectory()){
                    li.classList = 'dir'
                    sp.addEventListener('click',(e)=>{                        
                        if (li.classList.contains('loaded')){
                            li.classList.toggle('active')
                            return;
                        }
                        li.appendChild(GetListing(filePath))
                        li.classList.add('loaded','active')            
                    });
                }
                if (stat.isFile()){
                    li.classList = 'file'
                }

                li.appendChild(sp)
                frag.appendChild(li)
            });
        }        
    });  
    return frag;   
}


function CreateTree(dst, path){
    dst.innerHTML = ""
    dst.appendChild(GetListing(path))    
}

function GetInitPath(){
    //get electron default.
    userDataPath = userDataPath
    //check store for saved directory
    //update if needed 
}

function NewFileTree(){
    console.log(remote.getGlobal('AppStore'));
    let sidebar = document.getElementById('file-tree-explorer');
    let rt = document.createElement('div')
    let filetree = document.createElement('div')
    let rtsp = document.createElement('span')
    rtsp.innerHTML = userDataPath
    rt.appendChild(rtsp)
    rt.classList = 'root'

    filetree.setAttribute('id','filetree')
    CreateTree(filetree,userDataPath)
    rt.setAttribute('data-path', userDataPath)
    rt.addEventListener('click', _ => {
        dialog.showOpenDialog({ properties: ['openDirectory'] },(path)=>{
            if (typeof path == 'undefined'){
                return
            }
            path = path.toString()
            rtsp.innerHTML = path;
            CreateTree(filetree,path)
        })
    })

    sidebar.appendChild(rt)
    sidebar.appendChild(filetree)    
}

export {NewFileTree}
