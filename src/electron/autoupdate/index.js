const { autoUpdater } = require("electron-updater")

function AutoUpdate(){
  
  autoUpdater.requestHeaders = { "PRIVATE-TOKEN": "SOMETOKENGOESHERE" };
  autoUpdater.on('checking-for-update', () => {
    console.log('Checking for update...');
  })
  autoUpdater.on('update-available', (ev, info) => {
    console.log('Update available.');
  })
  autoUpdater.on('update-not-available', (ev, info) => {
    console.log('Update not available.');
  })
  autoUpdater.on('error', (ev, err) => {
    console.log('Error in auto-updater.');
  })
  autoUpdater.on('download-progress', (ev, progressObj) => {
    console.log('Download progress...');
  })
  autoUpdater.on('update-downloaded', (ev, info) => {
    console.log('Update downloaded; will install in 5 seconds');
  });
  autoUpdater.on('update-downloaded', (ev, info) => {
    // Wait 5 seconds, then quit and install
    // In your application, you don't need to wait 5 seconds.
    // You could call autoUpdater.quitAndInstall(); immediately
    setTimeout(function() {
      autoUpdater.quitAndInstall();  
    }, 5000)
  })
  autoUpdater.checkForUpdates()
  
}


export {AutoUpdate}