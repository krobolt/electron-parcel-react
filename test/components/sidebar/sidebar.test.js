var assert = require('assert');
var expect = require('chai').expect
import {Sidebar, RemoveActive, Update} from '../../../src/components/sidebar'

describe('sidebar.js', function() {
  describe('Class Sidebar (construct)', function() {
    
    it('should return error unable to find dragbar', function() {
      let sdiv = MockResize()
      expect(() => {new Sidebar(sdiv, null)} ).to.throw("unable to find dragbar");
    });
    
    it('should contain screen values', function() {
      let sdiv = MockSidebar()
      const expected = {
        x: 100,
        y:100
      }
      let sidebar = new Sidebar(sdiv, expected)    
      assert.equal(sidebar.screen, expected);
    });

    it('should bind mousedown event', function() {

      const screenVal = {}
      let sdiv = MockResize()
      let drag = MockDragbar()
      sdiv.appendChild(drag)

      let sidebar = new Sidebar(sdiv, screenVal)    
      var eventOver = document.createEvent("Event");
      eventOver.initEvent("mousedown", false, true); 
      drag.dispatchEvent(eventOver);
      assert.equal(sidebar.events.mousedown, true);
    });
  });



  describe('RemoveActive()', function() {
    it('should remove active class from div', function() {
      let div = document.createElement('div')
      div.classList = 'test active' 
      div = RemoveActive(div)
      assert.equal(div.classList, 'test');
    });   
  });

  describe('Update()', function() {
    it('should update style side effects', function() {
      
      let activeSidebar = {
        side: document.createElement('div'),
        main: document.createElement('div'),
        shift: 100,
      }

      activeSidebar = Update(activeSidebar)
      assert.equal(activeSidebar.side.getAttribute('style'), "width:"+activeSidebar.shift+"px;");
      assert.equal(activeSidebar.main.getAttribute('style'), "left:"+activeSidebar.shift+"px;");

    });   
  });


});



function MockDragbar(){
  let drag = document.createElement('div')
  drag.classList.add('dragbar')
  return drag
}

function MockResize(){
  let sdiv = document.createElement('div');
  sdiv.classList.add('horizontal-resize')
  return sdiv
}

function MockSidebar(){
  let sdiv = MockResize()
  let bar = MockDragbar()
  sdiv.appendChild(bar)
  return sdiv;
}